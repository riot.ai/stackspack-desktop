export function UPDATE_CONTACT(state, payload) {
  let idx = state.contacts.findIndex(({ id }) => id === payload.id);
  let updated = Object.assign({}, state.contacts[idx], payload.update);
  state.contacts.splice(idx, 1, updated);
}

export function ADD_CONTACT(state, contact) {
  state.contacts.unshift(contact);
}
