export default function() {
  return {
    btcAddress: "",
    stxAddress: "",
    btcNode: "",
    stxNode: "",
    lang: "en-US",
    sshKey: "",
    nodeUsername: "",
    nodePassword: ""
  };
}
