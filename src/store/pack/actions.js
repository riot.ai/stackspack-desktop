export function updateContact(context, payload) {
  return new Promise((resolve, reject) => {
    try {
      context.commit("UPDATE_CONTACT", payload);
      resolve("success");
    } catch (err) {
      reject(err);
    }
  });
}

export function addContact(context, contact) {
  return new Promise((resolve, reject) => {
    try {
      context.commit("ADD_CONTACT", contact);
      resolve("success");
    } catch (err) {
      reject(err);
    }
  });
}
